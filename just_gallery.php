<?php
/*
Plugin Name: Just gallery
*/
function just_gallery_shortcode( $attr ) {
	$post = get_post();

	static $instance = 0;
	$instance++;

	if ( ! empty( $attr['ids'] ) ) {
		// 'ids' is explicitly ordered, unless you specify otherwise.
		if ( empty( $attr['orderby'] ) ) {
			$attr['orderby'] = 'post__in';
		}
		$attr['include'] = $attr['ids'];
	}
	$output = apply_filters( 'post_gallery', '', $attr, $instance );
	if ( $output != '' ) {
		return $output;
	}
	$atts = shortcode_atts( array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post ? $post->ID : 0,
		'itemtag'    => 'figure' ,
		'icontag'    => 'div',
		'captiontag' => 'figcaption',
		'columns'    => 3,
		'size'       => 'medium',
		'include'    => '',
		'exclude'    => '',
		'link'       => 'file'
  ), $attr, 'gallery' );

$id = intval( $atts['id'] );
if ( ! empty( $atts['include'] ) ) {
		$_attachments = get_posts(
		array(
			'include' => $atts['include'],
			'post_status' => 'inherit',
			'post_type' => 'attachment',
			'post_mime_type' => 'image',
			'order' => $atts['order'],
			'orderby' => $atts['orderby']
			)
		);
		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( ! empty( $atts['exclude'] ) ) {
		$attachments = get_children(
			array(
				'post_parent' => $id,
				'exclude' => $atts['exclude'],
				'post_status' => 'inherit',
				'post_type' => 'attachment',
				'post_mime_type' => 'image',
				'order' => $atts['order'],
				'orderby' => $atts['orderby']
			)
		);
	} else {
		$attachments = get_children(
			array(
				'post_parent' => $id,
				'post_status' => 'inherit',
				'post_type' => 'attachment',
				'post_mime_type' => 'image',
				'order' => $atts['order'],
				'orderby' => $atts['orderby']
			)
		);
	}
if ( empty($attachments )) {
  return '';
}

if ( is_feed() ) {
  $output = "\n";
  foreach ( $attachments as $attachment ) {
    $output .= wp_get_attachment_link( $attachment->ID, $atts['size'], true ) . "\n";
  }
  return $output;
}

  $images = [];
  foreach($attachments as $attachment) {
    $images[] = array('src' => wp_get_attachment_url( $attachment->ID ), 'title' => $attachment->post_excerpt, "data" => wp_get_attachment_metadata($attachment->ID));
  }
  $context['settings'] = $atts;
  $context['settings']['instance'] = $instance;
  $context['images'] = $images;
  return Timber::compile('views/just_gallery.twig', $context);
}

// Replace default gallery with Just gallery
remove_shortcode('gallery', 'gallery_shortcode');
add_shortcode('gallery', 'just_gallery_shortcode');

function just_gallery_default_type_settings( $settings ) {
  unset($settings['galleryDefaults']);
 // $settings['galleryDefaults']['link'] = 'file';
  return $settings;
}
add_filter( 'media_view_settings', 'just_gallery_default_type_settings');

// Add js and css
add_action('wp_enqueue_scripts', function() {
  wp_enqueue_style( 'lightgllery', plugin_dir_url(__FILE__) . 'node_modules/lightgallery/dist/css/lightgallery.min.css');
//  wp_enqueue_style( 'blueimp', plugin_dir_url(__FILE__) . 'blueimp/css/blueimp-gallery.min.css');
  // wp_enqueue_style( 'just_gallery_blueimp', plugin_dir_url(__FILE__) . 'css/just_gallery_blueimp.css');
//   wp_enqueue_style( 'photoswipe', plugin_dir_url(__FILE__) . 'node_modules/photoswipe/dist/photoswipe.css');
  # wp_enqueue_style( 'photoswipe', plugin_dir_url(__FILE__) . 'node_modules/photoswipe/dist/default-skin/default-skin.css');
   wp_enqueue_style( 'just_gallery', plugin_dir_url(__FILE__) . 'component.css');
   wp_enqueue_script('masonry', plugin_dir_url(__FILE__) . 'node_modules/masonry-layout/dist/masonry.pkgd.min.js', array('jquery'));
   wp_enqueue_script('imageloaded', plugin_dir_url(__FILE__) . 'node_modules/imagesloaded/imagesloaded.pkgd.min.js');
   wp_enqueue_script('animOnScroll', plugin_dir_url(__FILE__) . 'animOnScroll.js', array('masonry'));
   wp_enqueue_script('classie', plugin_dir_url(__FILE__) . 'classie.js');
   wp_enqueue_script('modernizr', plugin_dir_url(__FILE__) . 'modernizr.js');
#   wp_enqueue_script('photoswipe', plugin_dir_url(__FILE__) . 'node_modules/photoswipe/dist/photoswipe.min.js#');
#   wp_enqueue_script('photoswipe', plugin_dir_url(__FILE__) . 'node_modules/photoswipe/dist/photoswipe-ui-default.min.js');
 #  wp_enqueue_script('just_gallery', plugin_dir_url(__FILE__) . 'just_gallery.js', array('jquery'));
 #  wp_enqueue_script('jquery_photoswipe', plugin_dir_url(__FILE__) . 'node_modules/jquery.photoswipe/dist/jquery.photVVoswipe-global.js', array('just_gallery'));
   wp_enqueue_script('lightgallery', plugin_dir_url(__FILE__) . 'node_modules/lightgallery/dist/js/lightgallery.min.js ', array('jquery'));
   wp_enqueue_script('just_gallery', plugin_dir_url(__FILE__) . 'just_gallery.js', array('jquery', 'lightgallery'));
   wp_enqueue_script('lightgallery_pager', plugin_dir_url(__FILE__) . 'node_modules/lg-pager/dist/lg-pager.min.js', array('lightgallery'));
   wp_enqueue_script('lightgallery_social', plugin_dir_url(__FILE__) . 'node_modules/lg-share/dist/lg-share.min.js', array('lightgallery'));
   wp_enqueue_script('lightgallery_hash', plugin_dir_url(__FILE__) . 'node_modules/lg-hash/dist/lg-hash.min.js', array('lightgallery'));
   wp_enqueue_script('lightgallery_zoom', plugin_dir_url(__FILE__) . 'node_modules/lg-zoom/dist/lg-zoom.min.js', array('lightgallery'));
});

?>
