jQuery(document).ready(function($) {
    $('.navbar .dropdown').hover(function() {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
    }, function() {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(105);
    });

    $('.gallery').find("a").attr("data-gallery", "#blueimp-gallery");
    $('body').append('<div id="blueimp-gallery" class="blueimp-gallery">');
    $('#blueimp-gallery').addClass("blueimp-gallery-controls");
    $('#blueimp-gallery').attr("data-carousel", "true");
    $('#blueimp-gallery').attr("data-continuous", "false");
    $('#blueimp-gallery').attr("data-hidePageScrollbars", "false");
    $('#blueimp-gallery').attr("data-startSlideshow", "falsoe");
    $('#blueimp-gallery').attr("data-slideshowInterval", 500);
    $('#blueimp-gallery').attr("data-transitionSpeed", 400);
    $('#blueimp-gallery').append('<div class="slides" />');
    $('#blueimp-gallery').append('<h3 class="title" />');
    $('#blueimp-gallery').append('<a class="prev">‹</a>');
    $('#blueimp-gallery').append('<a class="next">›</a>');
    $('#blueimp-gallery').append('<a class="close">x</a>');
    $('#blueimp-gallery').append('<a class="play-pause">›</a>');
    $('#blueimp-gallery').append('<ol class="indicator"></ol>');
});
